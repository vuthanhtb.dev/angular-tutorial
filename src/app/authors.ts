export interface Author {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  gender: string;
  ipAddress: string;
}

export const authors = [
  {
    id: 1,
    firstName: 'Terry',
    lastName: 'Medhurst',
    email: 'atuny0@sohu.com',
    gender: 'male',
    ipAddress: '99.180.237.33',
  },
  {
    id: 2,
    firstName: 'Sheldon',
    lastName: 'Quigley',
    email: 'hbingley1@plala.or.jp',
    gender: 'male',
    ipAddress: '99.168.210.30',
  },
  {
    id: 3,
    firstName: 'Terrill',
    lastName: 'Hills',
    email: 'rshawe2@51.la',
    gender: 'male',
    ipAddress: '168.170.23.12',
  },
  {
    id: 4,
    firstName: 'Miles',
    lastName: 'Cummerata',
    email: 'yraigatt3@nature.co',
    gender: 'male',
    ipAddress: '167.12.12.11',
  },
  {
    id: 5,
    firstName: 'Mavis',
    lastName: 'Schultz',
    email: 'kmeus4@upenn.edu',
    gender: 'male',
    ipAddress: '99.120.23.2',
  },
  {
    id: 6,
    firstName: 'Alison',
    lastName: 'Reichert',
    email: 'treleven5@nhs.uk',
    gender: 'female',
    ipAddress: '99.11.101.1',
  },
  {
    id: 7,
    firstName: 'Oleta',
    lastName: 'Abbott',
    email: 'dpettegre6@columbia.edu',
    gender: 'female',
    ipAddress: '168.120.101.100',
  },
];
