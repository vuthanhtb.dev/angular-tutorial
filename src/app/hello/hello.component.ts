import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-hello',
  templateUrl: './hello.component.html',
  styleUrls: ['./hello.component.css']
})

export class HelloComponent implements OnInit {
  @Input() name: string | undefined;

  ngOnInit(): void {
    console.log('app-hello --> init')
  }

  ngOnDestroy(): void {
    console.log('app-hello --> destroy')
  }

  isDanger = false;
  isWarring = false;
  classes = 'box red-border yellow-background';
}
