import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-progress-bar',
  templateUrl: './progress-bar.component.html',
  styleUrls: ['./progress-bar.component.css']
})
export class ProgressBarComponent implements OnInit, OnChanges {
  @Input() set progress(val: number) {
    // validation for validation
    console.log('set', {progress: val})
    this._progress = val;
  };
  private _progress = 50;
  get progress() {
    return this._progress;
  }

  @Input('progress-color') progressColor = 'tomato';
  @Input() backgroundColor = '#CCC';

  constructor() {
    console.log('constructor', {
      progress: this.progress,
      progressColor: this.progressColor,
      backgroundColor: this.backgroundColor
    });
  }

  ngOnInit(): void {
    console.log('ngOnInit', {
      progress: this.progress,
      progressColor: this.progressColor,
      backgroundColor: this.backgroundColor
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    // validation for validation
    console.log('ngOnChanges', {
      progress: this.progress,
      progressColor: this.progressColor,
      backgroundColor: this.backgroundColor
    });
  }

}
